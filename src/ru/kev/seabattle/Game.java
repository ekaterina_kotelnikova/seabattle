package ru.kev.seabattle;

import java.io.IOException;


/**
 * Класс для реализации игры "Морской бой" на одномерном поле
 *
 * @author Kotelnikova E.V. group 15IT20
 */

public class Game {
    private static int tries = 0;

    public static void main(String[] args) throws IOException {
        prepare();
        game();
        finish();
    }

    /**
     * Метод, заканчивающий игру. Выводит количество попыток, за которые пользователь потопит корабль.
     */

    private static void finish() {
        System.out.println("Чтобы потопить корабль вам потребовалось попыток: " + tries);
    }

    /**
     * Метод для реализации игры, в котором происходит повторный ввод координаты пользователем, пока он не потопит корабль.
     *
     * @return количество попыток
     * @throws IOException ошибка ввода-вывода
     */
    private static int game() throws IOException {
        String result;
        System.out.println("Сделайте вашу попытку потопить корабль: ");
        do {
            tries++;
            result = Ship.shoots(GameHelper.input());
            System.out.println(result);
        } while (!result.equals("Убил!"));
        return tries;
    }

    /**
     * Метод для подготовки к игре, ставит корабль на поле.
     */

    private static void prepare() {
        Ship ship = new Ship();
        int beginPoint = (int) (Math.random() * 8);
        ship.setLocation(beginPoint);
        System.out.println(ship.toString());
    }

}
