package ru.kev.seabattle;

import java.util.ArrayList;

/**
 * Класс, описывающий корабль
 *
 * @Kotelnikova E.V. group 15IT20
 */

public class Ship {
    private static ArrayList<String> location = new ArrayList<>();

    public void setLocation(int beginPoint) {
        location.add(Integer.toString(beginPoint));
        location.add(Integer.toString(beginPoint + 1));
        location.add(Integer.toString(beginPoint + 2));
    }

    public ArrayList<String> getLocation() {
        return location;
    }

    /**
     * Метод для обработки выстрела пользователя
     *
     * @param shot выстрел пользователя
     * @return текстовая константа "Убил!", "Ранил!", "Увы, мимо..."
     */

    public static String shoots(String shot) {
        int index = location.indexOf(shot);
        String result = "Увы, мимо...";
        if (index != -1) {
            location.remove(index);
            result = location.isEmpty()? "Убил!": "Ранил!";
        } return result;
    }

    @Override
    public String toString() {
        return "Корабль { " +
                "расположение = " + location +
                '}';
    }
}
