package ru.kev.seabattle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Класс для ввода данных с учётом масштабирования программы и проверкой на корректность
 *
 * @Kotelnikova E.V. group 15IT20
 */
public class GameHelper {
    private static String shot;

    /**
     * Метод для ввода данных с клавиатуры
     * @return координата выстрела
     * @throws IOException ошибка ввода-вывода
     */
    public static String input() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        shot = reader.readLine();
        shot = check();
        return shot;
    }

    /**
     * Метод проверяет введённые данные, которые должны быть числом от 0 до 9
     * @return корректная координата выстрела
     * @throws IOException ошибка ввода-вывода
     */
    private static String check() throws IOException {
        if (!shot.matches("[\\d]{1}")) {
            System.err.println("Неправильный формат данных. Введите заново.");
            shot = input();
        }
        return shot;
    }

    public static String getShot() {
        return shot;
    }
}
